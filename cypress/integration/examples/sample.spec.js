/**
    First Test
 */
describe('First Test', () => {
    it('Simple Comparison', () => {
        expect(true).to.equal(true);
    });
});

/**
    This test indicates that if the project is successfully loaded or not.
 */
describe('Home Page', () => {
    it('Successfully Loaded', () => {
        cy.visit('http://localhost:4200');
    });
});

/**
     This test indicates if routing is properly working also get an input, type into it and verify that the value has been updated
 */
describe('Routing Process and getting inputs', () => {
    it('Successfully Redirected to Form Component', () => {

        // 1. Click redirect button
        cy.get('.redirectionButton')
            .click()

        // 2. Check redirected URL
        cy.url()
            .should('include', '/forms');

        // 3. Check if submit button firstly is disabled
        cy.get('#submitButton')
            .should('have.disabled', true)

        // 4. Complete inputs with some text
        cy.get('#firstName')
            .type('MyFirstName')
            .should('have.value', 'MyFirstName')

        cy.get('#lastName')
            .type('MyLastName')
            .should('have.value', 'MyLastName')

        cy.get('#email')
            .type('test@test.com')
            .should('have.value', 'test@test.com')

        cy.get('#address')
            .type('TestAddress')
            .should('have.value', 'TestAddress')

        // 5. Click submit button (another way of getting a element)
        cy.contains('Submit').click().then(() => {
            alert('Please check CONSOLE LOG');
        })

        // 5. Check if form is finally disabled after submission
        cy.get('#firstName')
            .should('be.disabled')

        cy.get('#lastName')
            .should('be.disabled')

        cy.get('#email')
            .should('be.disabled')

        cy.get('#address')
            .should('be.disabled')
    });
});







