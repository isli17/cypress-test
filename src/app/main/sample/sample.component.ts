import { Component } from '@angular/core';

import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from '../../translation/en';
import {Router} from '@angular/router';

@Component({
    selector   : 'sample',
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss']
})
export class SampleComponent
{
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     * @param _router
     */
    constructor(
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _router: Router
    )
    {
        this._fuseTranslationLoaderService.loadTranslations(english);
    }

    redirect(): void {
        this._router.navigate(['./forms']);
    }
}
