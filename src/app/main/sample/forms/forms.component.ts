import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  userForm!: FormGroup;

  constructor() { }

  ngOnInit(): void {
    // 1. Form Building
    this.buildForm();
  }

  /**
   * buildForm
   * This function is used to build our Form
   */
  private buildForm(): void {
    this.userForm = new FormGroup ({
      firstName: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      lastName: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      email: new FormControl('', Validators.maxLength(300)),
      address: new FormControl('', Validators.maxLength(100))
    });
  }

  /**
   * saveUser
   * This function is used to send user data to the server side.
   */
  saveUser(): void {
    // 1. Disable Form
    this.userForm.disable();

    // 2. Get form Values and do something
    console.log(this.userForm.getRawValue());



  }
}
