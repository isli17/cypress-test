import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { SampleComponent } from './sample.component';
import { MatButtonModule } from '@angular/material/button';
import { FormsComponent } from './forms/forms.component';
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";

const routes = [
    {
        path     : 'sample',
        component: SampleComponent
    },
    {
        path     : 'forms',
        component: FormsComponent
    }
];

@NgModule({
    declarations: [
        SampleComponent,
        FormsComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule
    ],
    exports: [
        SampleComponent,
        FormsComponent
    ]
})

export class SampleModule
{
}
