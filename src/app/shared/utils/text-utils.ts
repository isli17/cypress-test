export class TextUtils {

    public static capitalize(text): string {
        return text.charAt(0).toUpperCase() + text.slice(1)
    }

    public static replaceTrChar(str): string {
        const chars = {
            'ş': 's',
            'ğ': 'g',
            'ü': 'u',
            'ı': 'i',
            'ö': 'o',
            'ç': 'c',
            'Ş': 'S',
            'Ğ': 'G',
            'Ü': 'U',
            'I': 'I',
            'İ': 'I',
            'Ö': 'O',
            'Ç': 'C'
        };
        let txt = '';

        for (let i = 0; i < str.length; i++) {
            chars[str[i]] ? txt += chars[str[i]] : txt += str[i];
        }
        return txt;
    }

    public static strToCamelCase(str: string): string {
        return this.replaceTrChar(str).toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
    }

    public static strToPascalCase(str: string): string {
        return this.capitalize(TextUtils.strToCamelCase(str));
    }

    public static strToHyphen(str: string): string {
        return str.replace(/ +/g, '-').toLowerCase();
    }

}
