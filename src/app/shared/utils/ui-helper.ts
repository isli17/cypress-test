import { Injectable } from '@angular/core';
import {AbstractControl, ValidatorFn} from '@angular/forms';

@Injectable()
export class UIHelper {

    constructor() {
    }

    /**
     * autocompleteStringValidator
     * Validate Options from Array of strings
     * To validate autocomplete against an array of string options, the validator may accept the array of options and check if the control value is included.
     * Usage: The validator could be added to a FormControl along with other built-in validators such as Validators.required
     */
    autocompleteStringValidator(validOptions: Array<string>): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            if (validOptions.indexOf(control.value) !== -1) {
                return null;  /* valid option selected */
            }
            return { 'invalidAutocompleteString': { value: control.value } };
        };
    }

    /**
     * autocompleteObjectValidator
     * Validate Options from Array of Objects
     * To validate autocomplete against an array of object options, the validator can leverage the fact that the control.value will only
     * be a string if a valid Object option has not been selected.
     * Usage: The validator could be added to a FormControl along with other built-in validators such as Validators.required
     */
    autocompleteObjectValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            if (typeof control.value === 'string') {
                return { 'invalidAutocompleteObject': { value: control.value } };
            }
            return null;  /* valid option selected */
        };
    }
}
