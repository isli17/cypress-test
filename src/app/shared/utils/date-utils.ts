import * as _moment from 'moment';

const moment = _moment;

export class DateUtils {
    public static toString(date: any, format: string = 'YYYY-MM-DD'): string {
        if (moment.isMoment(date)) {
            return date.format(format);
        }

        return moment(date).format(format);
    }

    public static getCreatedDateWithLocalDateTime(input = new Date(), locale = 'tr-TR', timeZone: string = 'Europe/Istanbul'): string {
        const turkishDate = input.toLocaleString(locale, {timeZone: timeZone});
        const date = turkishDate.split(' ')[0].split('.');
        const time = turkishDate.split(' ')[1];

        /*24.03.2020 16:07:24*/
        /*2020-03-24T16:04:01*/
        return date[2] + '-' + date[1] + '-' + date[0] + 'T' + time;
    }

}
