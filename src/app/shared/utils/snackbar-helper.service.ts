import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {FuseTranslationLoaderService} from '../../../@fuse/services/translation-loader.service';

@Injectable({
    providedIn: 'root'
})
export class SnackbarHelper {

    constructor(
        private _snackBar: MatSnackBar,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService) {
    }

    public showNotification(translationKey: string): void {
        // Notification
        this._snackBar.open(this._fuseTranslationLoaderService.translate(translationKey), 'OK',
            {
                verticalPosition: 'top',
                duration: 3000
            }
        );
    }
}
