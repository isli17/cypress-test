export class ValidateKeyPress {

    static onKeyPressNumber(event: any): boolean {
        const pattern = /^[0-9]+$/; // /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
        return this.onKeyPress(event, pattern);
    }

    static onKeyPressName(event: any): boolean {
        const pattern = /^([^0-9]*)$/;
        // const pattern = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/
        return this.onKeyPress(event, pattern);
    }

    // TODO: Directive taşınacak.
    static onKeyDownFirstLetterControl(event: any): boolean {

        if (event.target.value.length === 0 && event.key[0] === ' ') {
            event.preventDefault();
            return false;
        }
        return true;
    }

    static onKeyPress(event: any, pattern): boolean {
        if (event.key != null && event.key !== '') {
            switch (event.key.toLowerCase()) {
                case 'metaleft':
                case 'delete':
                case 'arrowleft':
                case 'arrowright':
                case 'arrowdown':
                case 'arrowup':
                case 'enter':
                    return false;
            }
        }

        let inputChar = String.fromCharCode(event.charCode || event.keyCode);
        if (!(event.charCode || event.keyCode)) {
            inputChar = event.clipboardData.getData('Text');
        }

        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();

            return false;
        }

        return true;
    }

    static onBackspacePressed(event: any): boolean {
        if (event.key != null && event.key !== '') {
            switch (event.key.toLowerCase()) {
                case 'backspace':
                    return true;
            }
        }

        return false;
    }
}
