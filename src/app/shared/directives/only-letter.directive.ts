import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[OnlyLetter]'
})
export class OnlyLetterDirective {

    constructor(private el: ElementRef) {
    }

    @Input() OnlyLetter: boolean;

    // tslint:disable-next-line: typedef
    @HostListener('keydown', ['$event']) onKeyDown(event) {
        const e = event as KeyboardEvent;

        const regex = /^[0-9]*$/;
        if (this.OnlyLetter) {
            if ([46, 8, 9, 27, 13, 110, 190, 32].indexOf(e.keyCode) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }

            if (!regex.test(e.key)) {
                return;
            } else {
                e.preventDefault();
            }

        }
    }
}
