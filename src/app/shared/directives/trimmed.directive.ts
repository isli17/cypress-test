import { Directive, HostListener } from '@angular/core';
@Directive({
    selector: '[Trimmed]'
})
export class TrimmedDirective {

    constructor() { }

    @HostListener('keydown', ['$event']) onKeyDown(event) {
        if (event.key === ' ' && event.target.value.length === 0) {
            event.preventDefault();
            return false;
        }
        return true;
    }

    @HostListener('input', [
        '$event.target',
        '$event.target.value',
    ])
    onInput(el: any, value: any): void {
        el.value = value.trimLeft();
    }

    @HostListener('textarea', [
        '$event.target',
        '$event.target.value',
    ])
    onTextarea(el: any, value: any): void {
        el.value = value.trimLeft();
    }

    @HostListener('paste', ['$event'])
    onPaste(event: ClipboardEvent) {
        event.preventDefault();
        let pastedInput: any = event.clipboardData
            .getData('text/plain').toString()
        pastedInput = pastedInput.trimLeft();

        document.execCommand('insertText', false, pastedInput);
    }

    @HostListener('drop', ['$event'])
    onDrop(event: DragEvent) {
        event.preventDefault();
        let textData: any = event.dataTransfer
            .getData('text').toString()
        textData = textData.trimLeft();
        document.execCommand('insertText', false, textData);
    }
}


