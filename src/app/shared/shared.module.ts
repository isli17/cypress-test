import { NgModule } from '@angular/core';
import { OnlyLetterDirective } from './directives/only-letter.directive';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { TrimmedDirective } from './directives/trimmed.directive';
import { OnlyDateDirective } from './directives/only-date.directive';

@NgModule({
    declarations: [
        OnlyLetterDirective,
        OnlyNumberDirective,
        TrimmedDirective,
        OnlyDateDirective
    ],
    imports: [],
    exports: [
        OnlyLetterDirective,
        OnlyNumberDirective,
        TrimmedDirective,
        OnlyDateDirective
    ]
})
export class SharedModule {
}
