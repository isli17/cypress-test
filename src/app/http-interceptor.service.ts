import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { FuseSplashScreenService } from '../@fuse/services/splash-screen.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    constructor(private snackbar: MatSnackBar,
                private translateService: TranslateService,
                private fuseSplashScreen: FuseSplashScreenService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {

                const errorStatusCode = error.status;
                let errorMsg = error.error.message ? error.error.message : '';

                if (errorStatusCode === 500 || errorStatusCode === 503 || errorStatusCode === 0) {
                    errorMsg = this.translateService.instant(`ERROR_MESSAGES.ERROR_500`);
                }

                this.snackbar.open(errorMsg, 'OK', {
                    duration: 3000
                });

                this.fuseSplashScreen.hide();

                console.warn('ERR: ', error);

                return throwError(error);
            })
        );
    }
}
